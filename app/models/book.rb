class Book < ActiveRecord::Base
  belongs_to :author
  belongs_to :category
  has_many :reviews
  has_many :order_items
end
