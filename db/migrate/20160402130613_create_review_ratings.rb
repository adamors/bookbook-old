class CreateReviewRatings < ActiveRecord::Migration
  def change
    create_table :review_ratings do |t|
      t.integer :review_id
      t.integer :user_id
      t.integer :score

      t.timestamps null: false
    end
  end
end
