class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.integer :book_id
      t.integer :user_id
      t.integer :order_id
      t.integer :price

      t.timestamps null: false
    end
  end
end
